# Uittreksel Zorgaanbiedergegevens - HealthcareproviderDetailsExcerptCredential
Hieronder staat een *voorbeeld* van de toepassing Uittreksel Zorgaanbiedergegevens JSONLD.
```json
{
  "@context": "https://kik-v.nl/provider/v1.json",
  "credentialSubject": {
    "id": "did:nuts:CCvypebjmBSWYnJ1CbCWJf1M8MqFRiJPNXx25a63bC8m",
    "organization": {
      "chamberOfCommerceNumber": "41072929",
      "name": "Stichting Verpleeghuis Bergweide"
    }
  },
  "issuanceDate": "2023-08-04T13:19:30.847099543Z",
  "issuer": "did:nuts:AXFwxMC5MD8rHeHcsEZUW6RLa67Gjen6vWAnc4mjUsCa",
  "type": "HealthcareproviderDetailsExcerptCredential",
  "visibility": "public"
}
```
